import React, { Component } from "react";
import {
  Col, Row, Input, Button,
} from "antd";
import { LogoutOutlined } from '@ant-design/icons';
import "./App.less";
import { CKEditor } from 'ckeditor4-react';

class App extends Component {
  state = {
    CONTENT: "dadfffdfdad",

    status: 0
  };

change(name,evt)
{
console.log(evt.editor.getData());
}


  render() {


    return (
      <div>
        <h2>Using CKEditor 4 in React</h2>
        <CKEditor
          editorUrl={process.env.PUBLIC_URL + "/ckeditor/ckeditor.js"}

          config=
          {{
            mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML',
          }}
          initData={this.state.CONTENT}
          

          type="classic"

          onInstanceReady={() => {
            alert('Editor is ready!');
          }}
           onChange={this.change.bind(this,"Answer")}
        />
        <Button type="primary" onClick={this.onlclickdisplay}>submit</Button>
      </div>
    );
  }
}


export default App;
